import java.util.Arrays;

public class myList<E> {
    //Size of list
    private int size = 0;

    //Default capacity of list is 10
    private static final int DEFAULT_CAPACITY = 10;

    //This array will store all elements added to list
    private Object elements[];

    //Default constructor
    public  myList() {
        elements = new Object[DEFAULT_CAPACITY];
    }

    //Add method
    public void addItem(E value) throws addItemFormatException {
            if (size == elements.length) {
                ensureCapacity();
            }
            elements[size++] = value;

    }
    // check value is in list?
    public boolean contains(Object var1) {
        return this.indexOf(var1) >= 0;
    }
    public int indexOf(Object var1) {
        for(int i = 0; i < this.size; ++i) {
            if (var1.equals(this.elements[i])) {
                return i;
            }
        }
        return -1;
    }
    //Get method
    public E getItem(int i) {
        if (i >= size || i < 0) {
            throw new IndexOutOfBoundsException("Index: " + i + ", Size " + i);
        }
        return (E) elements[i];
    }

    //Get Size of list
    public int size() {
        return size;
    }

    private void ensureCapacity() {
        int newSize = elements.length * 2;
        elements = Arrays.copyOf(elements, newSize);
    }

}
