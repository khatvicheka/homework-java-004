public class newList<E> {
    myList<E> list = new myList<>();
    public void addItem(E value) throws addItemFormatException{

        if (value==null){
            throw new   addItemFormatException("Inputted null value");
        }else if (list.contains(value)){
            throw new   addItemFormatException("Duplicate value : "+value);
        }
        else
            list.addItem(value);
    }

    public E getItem(int i){
        return list.getItem(i);
    }

    public int size() {
        return list.size();
    }
}
